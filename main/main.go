package main

import (
	"encoding/json"
	"errors"
	"flag"
	"log"
	"os"
	"strings"

	"ljrk.org/aax"
)

type Config struct {
	ActivationBytes string `json:"activation_bytes"`
}

/// Pirated from https://www.thepolyglotdeveloper.com/2017/04/load-json-configuration-file-golang-application/
func LoadConfiguration(file string) (Config, error) {
	var config Config

	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		return config, err
	}

	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	if err != nil {
		return config, err
	}

	if config.ActivationBytes == "" {
		return config, errors.New("ActivationBytes missing!")
	}

	return config, nil
}

var infile = flag.String("in", "test.aax", "input file name")
var outfile = flag.String("out", "test.mp3", "output file name")

func main() {
	parseArgs()

	config, err := LoadConfiguration("aax_config.json")
	if err != nil {
		log.Fatalf("Loading configuration failed: %v\n", err)
	}

	audiofile := aax.Input(*infile, config.ActivationBytes).Output(*outfile)

	audiobook, err := audiofile.Convert()
	if err != nil {
		log.Fatalf("Conversion failed: %v\n", err)
	}

	err = audiobook.Chapters()
	if err != nil {
		log.Fatalf("Creating chapters failed: %v\n", err)
	}
}

func parseArgs() {
	flag.Parse()

	if !strings.HasSuffix(strings.ToLower(*infile), ".aax") {
		log.Fatal("input file name must end with .aax")
	}
}
