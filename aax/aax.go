package aax

import (
	"fmt"

	ffmpeg "github.com/u2takey/ffmpeg-go"
)

///	Represents an Amazon AAX audiobook
type AAXFile struct {
	Filename        string
	ActivationBytes string
}

func Input(filename string, activationBytes string) *AAXFile {
	aaxfile := &AAXFile{
		Filename:        filename,
		ActivationBytes: activationBytes,
	}
	return aaxfile
}

func (a *AAXFile) CreateCoverFile(filename string) *AAXFile {
	input := ffmpeg.Input(a.Filename, ffmpeg.KwArgs{
		"activation_bytes": a.ActivationBytes,
	})
	output := input.
		Output(filename, ffmpeg.KwArgs{
			"codec:v": "copy",
		}).
		GlobalArgs("-an").
		OverWriteOutput()

	fmt.Println(output.GetArgs())
	err := output.Run()
	if err != nil {
		fmt.Println(err)
	}

	return a
}

///	Represents the (to be) decrypted audiofile
type Audiofile struct {
	AAXFile   *AAXFile
	Filename  string
	Overrides Overrides
}

type Overrides struct {
	Codec            string
	CompressionLevel int
	Bitrate          int
	Metadata         *Metadata
	Container        string
}

type Metadata struct {
	Title       string
	Artist      string
	AlbumArtist string
	Date        string
	Track       string
	Genre       string
	Copyright   string
	Description string
	Composer    string
	Publisher   string
}

func (a *AAXFile) Output(filename string) *Audiofile {
	audiofile := &Audiofile{
		Filename: filename,
		AAXFile:  a,
	}
	return audiofile
}

func (a *Audiofile) SetCodec(codec string) *Audiofile {
	a.Overrides.Codec = codec
	return a
}

func (a *Audiofile) CreateCoverFile(filename string) *Audiofile {
	a.AAXFile.CreateCoverFile(filename)
	return a
}

///	Represents the converted file ready for more postprocessing
type Audiobook struct {
	Audiofile   *Audiofile
	M3UPlaylist string
}

func (a *Audiofile) Convert() (*Audiobook, error) {
	input := ffmpeg.Input(a.AAXFile.Filename, ffmpeg.KwArgs{
		"activation_bytes": a.AAXFile.ActivationBytes,
	})
	output := input.
		Output(a.Filename).
		GlobalArgs("-vn").
		OverWriteOutput()

	fmt.Println(output.GetArgs())
	err := output.Run()
	if err != nil {
		return nil, err
	}

	audiobook := &Audiobook{
		Audiofile: a,
	}
	return audiobook, nil
}

func (a *Audiobook) SetM3UPlaylist(filename string) *Audiobook {
	a.M3UPlaylist = filename
	return a
}

func (a *Audiobook) Chapters() error {
	chapters, err := a.Audiofile.AAXFile.ProbeChapters()

	if err != nil {
		return err
	}

	fmt.Println(chapters)

	input := ffmpeg.Input(a.Audiofile.Filename)
	a.Audiofile.CreateCoverFile("cover.jpg")
	input2 := ffmpeg.Input("cover.jpg")
	for _, chap := range chapters {
		outputName := chap.Tags["title"] + ".mp3"
		output := ffmpeg.Output([]*ffmpeg.Stream{input, input2}, outputName, ffmpeg.KwArgs{
			"ss":           chap.StartTime,
			"to":           chap.EndTime,
			"acodec":       "copy",
			"map_chapters": -1,
		}).
			GlobalArgs("-map", "1:0").
			OverWriteOutput()

		fmt.Println(output.GetArgs())
		err := output.Run()
		if err != nil {
			return err
		}
	}

	return nil
}
